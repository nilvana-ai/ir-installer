volumes:
  inference-runtime-model:
  inference-runtime-influxdb:
  inference-runtime-mariadb:
services:
  mosquitto:
    image: quay.io/nilvana/mosquitto:latest
    ports:
      - "1883:1883"
      - "9001:9001"
      - "52060:1883"
      - "52061:9001"
    restart: on-failure:5
    logging:
      driver: "json-file"
      options:
          max-file: "7"
          max-size: "10m"
    labels:
      ai.nilvana.service.detach: true
  redis:
    image: quay.io/nilvana/redis:6.0
    command: bash -c "redis-server && sleep 5 && redis-cli FLUSHALL"
    restart: on-failure:5
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"
  mariadb:
    image: quay.io/nilvana/mariadb:10.6
    volumes:
      - "inference-runtime-mariadb:/var/lib/mysql"
    environment:
      - MARIADB_DATABASE=inference-runtime # keep this
  influxdb:
    image: quay.io/nilvana/influxdb:2.1
    volumes:
      - "inference-runtime-influxdb:/var/lib/influxdb2"
  inference-worker:
    image: quay.io/nilvana/nilvana-inference-runtime:2.1.0-inference-worker-arm64
    volumes:
      - "inference-runtime-model:/models"
    deploy:
      resources:
        reservations:
          devices:
          - driver: nvidia
            capabilities: [gpu, utility, compute]
    environment:
      - NVIDIA_VISIBLE_DEVICES=all
    shm_size: 1g
    ulimits:
      memlock: -1
      stack: 67108864
    restart: on-failure:5
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"
  frontend:
    image: quay.io/nilvana/nilvana-inference-runtime:2.1.0-frontend-arm64
    ports:
      - "52000:80"
    depends_on:
      - backend
      - mosquitto
    environment:
      - VERSION=2.1.0
      - MQTT_HOST=nilvana-mosquitto
      - BACKEND_HOST=ir-backend
    restart: on-failure:5
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"
  backend: # api-server
    image: quay.io/nilvana/nilvana-inference-runtime:2.1.0-server-nano
    ports:
      - "52001:80"
    depends_on:
      - redis
      - influxdb
      - mariadb
    volumes:
      - "inference-runtime-model:/models"
      - "$PWD:/license" # folder with license file
    restart: on-failure:5
    privileged: true
    environment:
      - TRITON_HOST=ir-inference-worker
      - INFER_GO_HOST=ir-inference-server
      - WORKER_PY_HOST=ir-task-worker
      - REDIS_HOST=ir-redis
      - MARIADB_HOST=ir-mariadb
      - INFLUX_HOST=ir-influxdb
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"
  task-worker: # worker-py
    image: quay.io/nilvana/nilvana-inference-runtime:2.1.0-task-worker-nano
    ports:
      - "52002:80"
    depends_on:
      - redis
      - influxdb
    volumes:
      - "inference-runtime-model:/models"
      - "$PWD:/license" # folder with license file
    deploy:
      resources:
        reservations:
          devices:
          - driver: nvidia
            capabilities: [gpu, utility, compute]
    environment:
      - NVIDIA_VISIBLE_DEVICES=all
      - REDIS_HOST=ir-redis
      - INFLUX_HOST=ir-influxdb
    restart: on-failure:5
    privileged: true
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"
  inference-server: # infer-go
    image: quay.io/nilvana/nilvana-inference-runtime:2.1.0-inference-server-nano
    ports:
      - "52010:80"
    volumes:
      - "inference-runtime-model:/models"
    environment:
      - TRITON_HOST=ir-inference-worker
      - WORKER_HOST=ir-task-worker
      - DB_HOST=ir-redis
      - MQTT_HOST=nilvana-mosquitto
    depends_on:
      - inference-worker
      - redis
      - mosquitto
    restart: on-failure:5
    logging:
      driver: "json-file"
      options:
        max-file: "7"
        max-size: "10m"